.. title: Features
.. slug: features
.. date: 2016-11-19 10:21:31 UTC+01:00
.. tags: 
.. category: 
.. link: 
.. description: 
.. type: text
.. pretty_url: False
.. nocomments: True

Storage Management and Monitoring
---------------------------------

The code is based on a modern and extensible architecture, built with proven web
technologies like `AngularJS <https://angularjs.org/>`_, `Bootstrap
<http://getbootstrap.com/>`_ and the `Django <https://www.djangoproject.com/>`_
web application framework.

* Wizard-based workflows for common tasks
* Supports multiple file systems for file-based storage management
  (`XFS <http://xfs.org>`_, `ext3/4 <https://ext4.wiki.kernel.org/>`_,
  `Btrfs <https://btrfs.wiki.kernel.org/>`_, `ZFS on Linux
  <http://zfsonlinux.org/>`_)
* `LVM2 <https://sourceware.org/lvm2/>`_ support
* Syncronous volume mirroring via `DRBD <http://www.drbd.org/>`_ ®
* CIFS (via `Samba <http://samba.org/>`_)
* NFS
* iSCSI and Fibre Channel (via `LIO <http://www.linux-iscsi.org/>`_)
* Volume snapshots (LVM2, ZFS)
* Automatic monitoring of performance data with `Nagios
  <https://www.nagios.org/>`_/`Icinga <https://www.icinga.com/>`_ and `PNP4Nagios
  <https://docs.pnp4nagios.org/>`_

Ceph Management and Monitoring
------------------------------

* Customizable `Ceph <https://ceph.com/>`_ Cluster dashboard (cluster health,
  cluster performance data)
* Supports managing multiple Ceph clusters from the same UI
* Ceph Pool management (view/create/delete) and Monitoring
* Ceph Pool Erasure code profile management
* RADOS Block device management (view/create/delete/map) and monitoring
* Object Storage Daemon (OSD) management (view)
* CRUSH Map editor

Other features
--------------

* :doc:`Installation packages <download>` for multiple Linux distributions
  (Debian/Ubuntu/RHEL/CentOS/SUSE)
* Multi-user support
* Multi-node support (one UI to manage multiple openATTIC instances)
* API Recorder for easy application development/debugging


Missing a feature?
------------------

openATTIC is under active development. If you're missing a particular feature,
please :doc:`let us know <get-involved>`!