.. title: Resize LVM inside an extended partition
.. slug: resize-lvm-inside-an-extended-partition
.. date: 2017-07-31 15:02:52 UTC+02:00
.. tags: lvm, resize, extended, filesystem, vm
.. category: 
.. link: 
.. description: 
.. type: text
.. author: Kai Wagner

Today I had to resize a logical volume inside an extended partition and I thought maybe this could be 
helpful for someone.

The resize is quite easy and you have to follow those few steps:

.. TEASER_END

First of all you have to extend the "physical" disk itself. The easiest scenario would be a virtual machine where you
could resize the disk at the CLI or via your VM management tool. Not all virtualization layer supports live resizing of a disk, so maybe
you have to shut down, resize and start the VM again::

	root@jenkins:~# fdisk /dev/vda

	Welcome to fdisk (util-linux 2.27.1).
	Changes will remain in memory only, until you decide to write them.
	Be careful before using the write command.


	Command (m for help): p
	Disk /dev/vda: 20 GiB, 21474836480 bytes, 41943040 sectors
	Units: sectors of 1 * 512 = 512 bytes
	Sector size (logical/physical): 512 bytes / 512 bytes
	I/O size (minimum/optimal): 512 bytes / 512 bytes
	Disklabel type: dos
	Disk identifier: 0x1d68d97a
	
	Device     Boot   Start      End  Sectors  Size Id Type
	/dev/vda1  *       2048   999423   997376  487M 83 Linux
	/dev/vda2       1001470 20969471 19968002  9.5G  5 Extended
	/dev/vda5       1001472 20969471 19968000  9.5G 8e Linux LVM
	
	Command (m for help): d   
	Partition number (1,2,5, default 5): 2
	
	Partition 2 has been deleted.
	
	Command (m for help): n
	Partition type
	   p   primary (1 primary, 0 extended, 3 free)
	   e   extended (container for logical partitions)
	Select (default p): p
	Partition number (2-4, default 2): 
	First sector (999424-41943039, default 999424): 1001472
	Last sector, +sectors or +size{K,M,G,T,P} (1001472-41943039, default 41943039): 
	
	Created a new partition 2 of type 'Linux' and of size 19.5 GiB.
	
	Command (m for help): t
	Partition number (1,2, default 2): 
	Partition type (type L to list all types): 8e
	
	Changed type of partition 'Linux' to 'Linux LVM'.
	
	Command (m for help): p
	Disk /dev/vda: 20 GiB, 21474836480 bytes, 41943040 sectors
	Units: sectors of 1 * 512 = 512 bytes
	Sector size (logical/physical): 512 bytes / 512 bytes
	I/O size (minimum/optimal): 512 bytes / 512 bytes
	Disklabel type: dos
	Disk identifier: 0x1d68d97a
	
	Device     Boot   Start      End  Sectors  Size Id Type
	/dev/vda1  *       2048   999423   997376  487M 83 Linux
	/dev/vda2       1001472 41943039 40941568 19.5G 8e Linux LVM
	
	Command (m for help): w
	The partition table has been altered.
	Calling ioctl() to re-read partition table.
	Re-reading the partition table failed.: Device or resource busy
	
	The kernel still uses the old table. The new table will be used at the next reboot or after you run partprobe(8) or kpartx(8).

	# reboot 

	root@jenkins:~# pvresize /dev/vda2
	Physical volume "/dev/vda2" changed
	1 physical volume(s) resized / 0 physical volume(s) not resized

	root@jenkins:~# lvresize -L+8.5G /dev/ubuntu-temp-vg/root 
	Size of logical volume ubuntu-temp-vg/root changed from 10.00 GiB (2560 extents) to 18.50 GiB (4736 extents).
	Logical volume root successfully resized.

	root@jenkins:~# resize2fs /dev/ubuntu-temp-vg/root 
	resize2fs 1.42.13 (17-May-2015)
	Filesystem at /dev/ubuntu-temp-vg/root is mounted on /; on-line resizing required
	old_desc_blocks = 1, new_desc_blocks = 2
	The filesystem on /dev/ubuntu-temp-vg/root is now 4849664 (4k) blocks long.
