.. title: openATTIC 2.0.19 beta has been released
.. slug: openattic-2019-beta-has-been-released
.. date: 2017-04-18 10:04:32 UTC+01:00
.. tags: announcement, collaboration, contributing, news, beta, opensource, release
.. category:
.. link:
.. description: Announcing the 2.0.19 beta release
.. type: text
.. author: Ricardo Marques

openATTIC 2.0.19 is now available. This is a minor release, backwards compatible, where we deliver on our promise to make openATTIC easy to use, faster and with a great GUI.

On the frontend, we improved the feedback given to the user with a better error handling, useful toasty notifications, and loading spinners. We also added the DRBD support to the graphical user interface.

Since it's spring time, we also did a bit of house cleaning! On the backend side, we removed some obsolete modules such as peering, IPMI and mdraid. We also extracted the XML RPC daemon and its related API because they have been replaced by our REST API some time ago.

Besides that, the backend offered even more room for improvement and fixes like the creation of erasure coded Ceph pools.

We believe that documentation is as important as code. That's why we made many documentation improvements for this release. For instance, we restructured and improved the format of our CHANGELOG inspired by the `Keep a Changelog <http://keepachangelog.com/>`_ project.

This release also contains two external contributions submitted by Uros Lates and David Díaz. Thank you, much appreciated!

We're happy to announce that this is also the last release built from our Mercurial repository, because :doc:`we have moved to... GIT! <openattic-code-repository-migrated-to-git>`
We also updated the "developer" documentation section to reflect the mercurial to git workflow changes.

During this release we moved our hardware to a new infrastructure which delayed the announcement of this version, 
we apologize for the inconvenience.

.. TEASER_END

Please note that 2.0.19 is still a **beta version**. As usual, handle with care
and make sure to create backups of your data!

Your feedback, ideas and bug reports are very welcome. If you would like to get
in touch with us, consider joining our `openATTIC Users
<https://groups.google.com/forum/#%21forum/openattic-users>`_ Google Group,
visit our `#openattic <irc://irc.freenode.org/#openattic>`_ channel on
`irc.freenode.net <http://webchat.freenode.net/?channels=openattic>`_ or leave
comments below this blog post.

See the list below for a more detailed change log and further references. The OP
codes in brackets refer to individual Jira issues that provide additional
details on each item. You can review these on our `public Jira instance
<http://tracker.openattic.org/>`_.

Changelog for 2.0.19:

Added
~~~~~
* WebUI: Display loading spinner icon on form submit to give the user feedback
  that something is happening in the background (:issue:`OP-1760`)
* WebUI: Automatically show toasty notifications for API errors.
  Thanks to Uros Lates for the contribution. (:issue:`OP-1631`)
* WebUI: Added DRBD support. (:issue:`OP-1201`)

Changed
~~~~~~~
* WebUI: Prevent the dashboard from notifying on a 401 request (:issue:`OP-1997`)
* WebUI: Using $log.error instead of throw on API errors (:issue:`OP-2003`)
* WebUI: Only use the items array in the ceph pool and rbd deletion process (:issue:`OP-1981`)
* WebUI/QA: Removed static names from common.js (:issue:`OP-1971`)
* Backend: REST API response header do now contain 'Cache-Control: no-cache'
  option (:issue:`OP-1402`)
* WebUI: Disabled successful task queue notifications (:issue:`OP-1961`)
* WebUI: Changed mouse cursor to "pointer" over clickable rows (:issue:`OP-1893`)
* WebUI: Dropdown button should not close when user clicks on a disabled option
  Thanks to David Díaz for the contribution. (:issue:`OP-940`)
* Documentation: Updated E2E section (changed test tool versions, :issue:`OP-1919`)
* Packaging: Stop and remove ``openattic-rpcd`` service when upgrading to
  version 2.0.19 (:issue:`OP-1900`)
* Development: Change API URL in a Vagrant box setup. (:issue:`OP-1913`)
* Development: Create various directories in a Vagrant box that are required
  at runtime. (:issue:`OP-1964`)
* Development: Add system user/group ``openattic`` in Vagrant boxes to be
  able to run Gatling unit tests. (:issue:`OP-1960`)
* Testing/QA: Improve Gatling tests to be able to execute them in a Vagrant
  box by making the API root part of the base URL configurable. Additionally
  more information are displayed if a test case fails. This could help to
  identify the problem. (:issue:`OP-1927`)
* Installation: Removed symlink from ``/etc/openattic/databases/pgsql.ini``
  (:issue:`OP-1822`)
* Documentation: Restructured and improved format of the CHANGELOG (:issue:`OP-1911`)
* Documentation: Add network preparation howto for Vagrant boxes. (:issue:`OP-2004`)

Fixed
~~~~~
* WebUI/QA: Fix random fails in various tests (:issue:`OP-1974`)
* WebUI: Fixed selection of zero items after deletion in a table (:issue:`OP-1980`)
* WebUI/QA: Fixed random fails in wizard tests (:issue:`OP-1969`)
* WebUI/QA: Fixed random fails in the task queue test (:issue:`OP-1970`)
* WebUI/QA: Fixed some e2e styling issues (:issue:`OP-1992`)
* WebUI: Fixed inconsistent state of rbd datatable actions (:issue:`OP-1954`)
* WebUI: Fixed the RBD utilization graphs (:issue:`OP-1945`)
* WebUI: Fixed ceph pagination issue (:issue:`OP-1925`)
* WebUI: Fixed bug when generating a new user API token. (:issue:`OP-1914`)
* WebUI: Readded the missing $q injection (:issue:`OP-1965`)
* Backend: Wait until the LV is ready when creating a DRBD resource. (:issue:`OP-1898`)
* Backend: Handle ValidationError exception correct, otherwise the error
  message isn't forwarded to the API caller. (:issue:`OP-1956`)
* Backend: The validation error message must be added as an array per field
  name. (:issue:`OP-1978`)
* Backend: Fixed Ceph RBD disk usage value (:issue:`OP-2008`)
* Installation: Fixed domain join - wrong order on ``oaconfig`` script (:issue:`OP-1209`)
* Installation: fixed location of the Nagios ``status.dat`` file on EL7 (:issue:`OP-1955`)
* Development: Remove useless code which breaks Gatling unit tests. (:issue:`OP-1963`)

Removed
~~~~~~~
* Backend: Removed ``oacli`` (:issue:`OP-1249`)
* Backend: Removed peering module (:issue:`OP-1252`)
* Backend: Removed RPC API files and RPCD module (:issue:`OP-1253`)
* Backend: The pkgapt module has been removed (:issue:`OP-1531`)
* Backend: Removed all RPCD and RPC API dependencies (:issue:`OP-1254`, :issue:`OP-1761`)
* Backend: The IPMI module has been removed (:issue:`OP-1772`)
* Backend: The mdraid module has been removed (:issue:`OP-1773`)
* Backend: The twraid module has been removed (:issue:`OP-1774`)
* Backend: Fix creation of erasure coded ceph pools (:issue:`OP-1885`)
* Installation: Ensure removal of obsolete RPM/DEB packages during update
  from a previous version (:issue:`OP-1968`)
