.. title: Meet the openATTIC Team at FrOSCon 2016
.. slug: meet-the-openattic-team-at-froscon-2016
.. date: 2016-07-08 12:38:38 UTC+02:00
.. tags: community, event, froscon, conference, community, collaboration, opensource, presentation
.. category:
.. link:
.. description: Announcing our presence at the FrOSCon conference
.. type: text
.. author: Lenz Grimmer

.. image:: https://www.froscon.de/fileadmin/images/logos/froscon_logo_print_bw.png

`FrOSCon <https://www.froscon.de/en/home/>`_, the annual Free and Open Source
Conference, takes place on August 20th and 21st in St. Augustin, Germany.

It is our great pleasure to be present there again - the openATTIC team will
have a booth in the exhibition area. `it-novum <http://it-novum.com/>`_,
the company that sponsors the openATTIC development, is also a proud `Silver
Sponsor <https://www.froscon.de/en/partners/>`_ of the event.

We'll be showcasing the latest development version of openATTIC, including the
latest Ceph management features.

You will also be able to participate in a quiz contest for two `Raspberry Pi 3
Model B <https://www.raspberrypi.org/products/raspberry-pi-3-model-b/>`_
single board computers, so please stop by at our exhibition desk if you want
to join!

I will also give an `overview talk about openATTIC 2.0
<https://programm.froscon.de/2016/events/1755.html>`_ (in German),
highlighting the latest changes and features as well as an outlook into future
development plans.

We look forward to being there again!
