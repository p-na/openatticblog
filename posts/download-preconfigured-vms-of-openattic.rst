.. title: Download Preconfigured VMs of openATTIC
.. slug: download-preconfigured-vms-of-openattic
.. date: 2016-07-28 18:06:23 UTC+02:00
.. tags: vms, preconfigured, openattic, download
.. category: 
.. link: 
.. description: 
.. type: text
.. author: Kai Wagner

It's now possible to `download <http://download.openattic.org/vms/>`_ and test openATTIC with already preconfigured virtual machines for KVM and VirtualBox.

There's Debian and Ubuntu available at the moment, but more coming soon.

See the `documentation <http://docs.openattic.org/2.0/install_guides/preconfigured_vms.html>`_ for further information::

	Please run "oaconfig install" the first time you've started the virtual machine. 
	Otherwise the WebUI will be empty.


