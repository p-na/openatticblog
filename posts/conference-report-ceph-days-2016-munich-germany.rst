.. title: Conference Report: Ceph Days 2016 Munich, Germany
.. slug: conference-report-ceph-days-2016-munich-germany
.. date: 2016-09-27 13:32:02 UTC+02:00
.. tags: conference, event, linux, ceph, openattic, storage, opensource
.. category:
.. link:
.. description: Summarizing last week's Ceph Day in Munich, Germany.
.. type: text
.. author: Lenz Grimmer

.. image:: http://ceph.com/wp-content/themes/ceph/images/logo_cephdays.png

Friday last week (23rd of September), I traveled to Munich, to attend and talk
about openATTIC at the `Ceph Day <http://ceph.com/cephdays/ceph-day-munich/>`_.

This Ceph Day was sponsored by Red Hat and SUSE and it was nice to see many
representatives of both companies attending and speaking about Ceph-related
topics. Even though the event was organized on short notice, almost 50 attendees
showed up.

I was there for the entire day and attended all sessions. I also took some
pictures, which can be found in `my Flickr set
<https://www.flickr.com/photos/lenzgr/albums/72157673306296210>`_.

Fortunately there was just a single track of presentations. While all talks
provided ample insight and new information about Ceph, I learned a lot from the
following sessions:

.. TEASER_END

Patrick McGarry (Director Ceph Community at Red Hat) provided a great historical
overview about Ceph's roots and how the project evolved over the years. I really
admire their decision to turn Ceph into a truly open source project by choosing
the LGPL and sharing the copyright with all contributors via the `Developer
Certificate of Origin <http://developercertificate.org/>`_ process that was
established by the Linux kernel community. This is very similar to the direction
that we chose for the openATTIC project last year. It was also impressive to get
some insight into the various `metrics <http://metrics.ceph.com/>`_ that the
Ceph project maintains about their activities.

.. media:: https://www.flickr.com/photos/lenzgr/29948266336/in/album-72157673306296210/

In his talk "Test Automation for iSCSI and VMware", Joshua Schmid (Software
Developer at SUSE) gave an overview about a `Perl-based simulator framework
<https://github.com/jschmid1/esxi_iscsi_loadgen>`_ for load, failure and basic
actions that can occur in a real world VMware / iSCSI / Ceph environment that he
has been working on. These test actually helped to discover a number of bugs in
the iSCSI implementation for Ceph. The choice of Perl was somewhat of a surprise
for me, but from what I gathered it was a result of the lack of a VMware SDK for
more modern languages.

.. media:: https://www.flickr.com/photos/lenzgr/29354731284/in/album-72157673306296210/

The following talk about "Delivering Cost-Effective, High Performance Ceph
Cluster" by Gert Pauwels (Solution Architect at Intel) was a tad bit too much of
an Intel product pitch in my opinion, but it was still interesting to learn more
about Intel's plans when it comes to evolving storage technology (SSD, NVMe,
etc.) and their vision of tomorrow's storage hiearchies in general.

The lunch break provided an excellent choice of food and beverages and left
sufficent time for networking and chatting with peers. I enjoyed being able to
touch base with some of the SUSE engineers that we collaborate with in our joint
development project of bringing more Ceph functionality to openATTIC.

Taco Scargo (Senior Solution Architect at Red Hat) then gave a presentation
about running Database Workloads (MySQL and derivatives in particular) on Ceph.
He presented a number of benchmarks and showed various tuning parameters that
helped to boost query performance on Ceph significantly. Not suprisingly,
increasing InnoDB's buffer pool had a huge impact, since this setting simply
caches I/O to circumvent the delay caused by slow storage devices. In my
personal opinion, running a database on Ceph is still a bad idea from a
performance POV, but it's nice to see that a lot of effort and research/testing
is spent on improving this.

.. media:: https://www.flickr.com/photos/lenzgr/29354879884/in/album-72157673306296210/

Next up was yours truly, I gave an introduction and overview to the openATTIC
and the current state of the Ceph management and monitoring functionality. I was
positively surprised by the amount of hands that went up when I asked who in the
audience had heard about openATTIC before - seems like all our recent activities
are getting noticed! I did not get much feedback or questions right after my talk,
but a number of attendees approached me afterwards to ask for some more details.

The slides of my talk can be found on `Slideshare
<http://www.slideshare.net/LenzGr/ceph-and-storage-management-with-openattic-ceph-day-munich-20160923>`_:

.. media:: http://www.slideshare.net/LenzGr/ceph-and-storage-management-with-openattic-ceph-day-munich-20160923

After a short coffee break, Patrick McGarry entered the stage again, to give an
update on various ongoing community activities and events in the Ceph ecosystem.
One noteworthy item was the announcement of "Cephalocon", a conference dedicated
to Ceph! A date and location have already been set: it will take place in Boston
(MA) on 23-25 August 2017. So mark your calendars! More details and a web site
will follow later.

The last speaker of the day was Allen Samuels (Engineering Fellow at SANDisk),
who gave an update on the state of development of the BlueStore storage backend,
which is supposed to replace the existing filestore backend due to performance
reasons. It still has some way to go, and the version that was shipped with the
Ceph "Jewel" release has already been oboleted by a new on-disk format. So for
now, one is probably better off with not enabling BlueStore on production Ceph
clusters. But it was certainly interesting to learn more about the motivation
and goals for developing this new storage backend to begin with!

.. media:: https://www.flickr.com/photos/lenzgr/29868471992/in/album-72157673306296210/

The Ceph day ended with a networking reception, which again provided an
opportunity to have a conversation with other attendees over a nice selection of
food and a choice of Bavarian beers.

I was very grateful for the opportunity to talk about openATTIC and to get an
insight into what's cooking in Ceph land. Thanks to Patrick McGarry and his team
for organizing this event!