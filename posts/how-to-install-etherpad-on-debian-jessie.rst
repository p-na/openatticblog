.. title: How to Install Etherpad on Debian Jessie
.. slug: how-to-install-etherpad-on-debian-jessie
.. date: 2016-06-29 15:16:04 UTC+02:00
.. tags:  howto install debian etherpad
.. category: 
.. link: 
.. description: 
.. type: text
.. author: Kai Wagner

This tutorial will show you how to install Etherpad on Debian Jessie.

The following command will install all necessary packages::

	apt-get install gzip git-core curl python libssl-dev pkg-config \ 
                build-essential python g++ make checkinstall \
                nginx-full mysql-server

.. TEASER_END

Next we need to compile and install nodejs::

	cd /usr/src/
	wget http://nodejs.org/dist/node-latest.tar.gz
	tar xzvf node-latest.tar.gz && cd node-v*
	./configure && checkinstall

When the dialog window opens, enter ‘3’ and remove the “v” in front of the version number.
Install nodejs with the following commandi::

	dpkg -i node_*

Create a MySQL or a MariaDB database ::

	CREATE DATABASE etherpad CHARACTER SET utf8 COLLATE utf8_general_ci;
	GRANT ALL PRIVILEGES ON etherpad.* TO etherpad@localhost IDENTIFIED BY '__yourPasswd__';
	FLUSH PRIVILEGES;
	\q

Install and configure Etherpad Lite

Go to the directory where you want to install Etherpad Lite and clone the git repository::

	cd /var/www/
	git clone git://github.com/ether/etherpad-lite.git

Edit the configuration file::

	cd etherpad-lite/
	cp settings.json.template settings.json
	vim settings.json

and change the following settings::

	// change the IP Address
	"ip": "127.0.0.1",
	
	//configure the connection settings
	 "dbType" : "mysql",
	 "dbSettings" : {
	   "user"    : "etherpad",
	   "host"    : "localhost",
	   "password": "__yourPassword__",
	   "database": "etherpad"
	 },
	
	// add admin user
	"users": {
	 "admin": {
	 "password": "__yourAdminPassword__",
	 "is_admin": true
	 }
	},

Create a system user ::

	adduser --system --home=/var/www/etherpad-lite/ --group etherpad
	chown -R etherpad: /var/www/etherpad-lite/

Start Etherpad Lite for the first time::

	su -c "/var/www/etherpad-lite/bin/run.sh" -s /bin/bash etherpad

If everything is OK, kill all processes belonging to the etherpad user::

	pkill -u etherpad

Create an init script using your favorite editor::

	#!/bin/sh
	
	### BEGIN INIT INFO
	# Provides:          etherpad-lite
	# Required-Start:    $local_fs $remote_fs $network $syslog
	# Required-Stop:     $local_fs $remote_fs $network $syslog
	# Default-Start:     2 3 4 5
	# Default-Stop:      0 1 6
	# Short-Description: starts etherpad lite
	# Description:       starts etherpad lite using start-stop-daemon
	### END INIT INFO
	
	PATH="/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin:/opt/node/bin"
	LOGFILE="/var/www/etherpad-lite/etherpad-lite.log"
	EPLITE_DIR="/var/www/etherpad-lite"
	EPLITE_BIN="bin/safeRun.sh"
	USER="etherpad"
	GROUP="etherpad"
	DESC="Etherpad Lite"
	NAME="etherpad-lite"
	
	set -e
	
	. /lib/lsb/init-functions
	
	start() {
	  echo "Starting $DESC... "
	
	    start-stop-daemon --start --chuid "$USER:$GROUP" --background --make-pidfile --pidfile /var/run/$NAME.pid --exec $EPLITE_DIR/$EPLITE_BIN -- $LOGFILE || true
	  echo "done"
	}
	
	#We need this function to ensure the whole process tree will be killed
	killtree() {
	    local _pid=$1
	    local _sig=${2-TERM}
	    for _child in $(ps -o pid --no-headers --ppid ${_pid}); do
	        killtree ${_child} ${_sig}
	    done
	    kill -${_sig} ${_pid}
	}
	
	stop() {
	  echo "Stopping $DESC... "
	   while test -d /proc/$(cat /var/run/$NAME.pid); do
	    killtree $(cat /var/run/$NAME.pid) 15
	    sleep 0.5
	  done
	  rm /var/run/$NAME.pid
	  echo "done"
	}
	
	status() {
	  status_of_proc -p /var/run/$NAME.pid "" "etherpad-lite" && exit 0 || exit $?
	}
	
	case "$1" in
	  start)
	      start
	      ;;
	  stop)
	    stop
	      ;;
	  restart)
	      stop
	      start
	      ;;
	  status)
	      status
	      ;;
	  *)
	      echo "Usage: $NAME {start|stop|restart|status}" >&2
	      exit 1
	      ;;
	esac
	
	exit 0

Start the script automatically on system boot::

	chmod +x /etc/init.d/etherpad-lite
	update-rc.d etherpad-lite defaults
	/etc/init.d/etherpad-lite start

Create a new virtual host (server block)::

	vim /etc/nginx/sites-available/domain.tld.conf
	
	server {
	 listen       80;
	 server_name  domain.tld;
	   location / {
	     proxy_pass        http://localhost:9001/;
	     proxy_set_header  Host $host;
	     proxy_buffering   off;
	   }
	 }
	
	ln -s /etc/nginx/sites-available/domain.tld /etc/nginx/sites-enabled/domain.tld
	/etc/init.d/nginx restart

In case you are using Apache, you can use the following virtual host directive::

	vim /etc/apache2/sites-available/domain.tld
	
	<VirtualHost *:80>
	    ServerName domain.tld
	    ServerSignature Off
	    <IfModule mod_proxy.c>
	        ProxyVia On
	        ProxyRequests Off
	        ProxyPass / http://127.0.0.1:9001/
	        ProxyPassReverse / http://127.0.0.1:9001/
	        ProxyPreserveHost on
	        <Proxy *>
	            Options FollowSymLinks MultiViews
	            AllowOverride All
	            Order allow,deny
	            allow from all
	        </Proxy>
	    </IfModule>
	</VirtualHost>
	
	ln -s /etc/apache2/sites-available/domain.tld /etc/apache2/sites-enabled/domain.tld
	a2enmod proxy proxy_http
	/etc/init.d/apache2 restart

That’s it! You can now access Etherpad Lite from your browser.

Thanks to rosehosting.com for the Wheezy tutorial.
