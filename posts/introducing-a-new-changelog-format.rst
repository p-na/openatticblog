.. title: Introducing a new CHANGELOG format
.. slug: introducing-a-new-changelog-format
.. date: 2017-03-10 18:56:07 UTC+01:00
.. tags: development, documentation
.. category:
.. link:
.. description: Describing the latest changes to the openATTIC CHANGELOG
.. type: text
.. author: Lenz Grimmer

A few days ago, I stumbled over an article on Opensource.com about "`How to
make release notes count
<https://opensource.com/article/17/3/how-to-improve-release-notes>`_".

The article is worth a read - it contains a number of useful suggestions on how
to structure the documentation that summarizes the changes that have taken place
in a software project's latest release.

Which was excellent timing, as we just merged a major overhaul of our own
project's CHANGELOG file (see :issue:`OP-1911` for details).

We adapted our `CHANGELOG
<https://bitbucket.org/openattic/openattic/src/default/CHANGELOG>`_ based on
suggestions made by the `Keep a Changelog
<http://keepachangelog.com/>`_ project, with the exception of using
reStructuredText for markup instead of Markdown.

To describe their impact on the project, changes are now grouped as follows:

* **Added** for new features.
* **Changed** for changes in existing functionality.
* **Deprecated** for once-stable features removed in upcoming releases.
* **Removed** for deprecated features removed in this release.
* **Fixed** for any bug fixes.
* **Security** to invite users to upgrade in case of vulnerabilities.

This will hopefully make it easier for our users to get an impression on the
changes to expect when updating to a newer version.

How do you like the new structure? `Please let us know
<http://openattic.org/get-involved.html>`_. Thanks!