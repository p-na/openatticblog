.. title: Demo currently unavailable
.. slug: demo-currently-unavailable
.. date: 2017-05-02 08:43:44 UTC+02:00
.. tags: community, demo, downtime, migration 
.. category: 
.. link: 
.. description: Background info about the status of demo.openattic.org
.. type: text
.. author: Kai Wagner

You might have noticed already that our live demo on `demo.openattic.org
<https://demo.openattic.org>`_ is down and not reachable at the moment.

This issue is caused by our hardware move to a new and more secure datacenter.
Right now, we're trying to figure out what's the best approach to make the
demo accessible again. This isn't as easy as before because we now have a
dedicated firewall and a dmz for external services.

Therefore the `demo.openattic.org <https://demo.openattic.org>`_ URL will be
redirected to `Demo currently unavailable <https://www.openattic.org/posts/demo-currently-unavailable/>`_ until we have a
running demo again.

Update: It's not as easy as expected. We still need to investigate into the best approach. Coming soon...
