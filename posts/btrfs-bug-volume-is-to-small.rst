.. title: BTRFS resize bug - volume is too small
.. slug: btrfs-bug-volume-is-to-small
.. date: 2016-05-12 16:48:05 UTC+02:00
.. tags: btrfs bug kernel
.. category: 
.. link: 
.. description: 
.. type: text
.. author: Kai Wagner

Today we tried to resize a btrfs volume and always get the following useless error::

	"ERROR: unable to resize '/media/test/' - Invalid argument"

We came to the idea, that this is maybe a hardcoded limit or problem, because we figured out that we can resize volumes which are bigger than 256MBit.

I have taken a look into the source code of btrfs and found something in the ioctl.c file in line 1584::

 1584        if (new_size < SZ_256M) {
 1585               ret = -EINVAL;
 1586               goto out_free;
 1587        }

After I talked to some guys from the btrfs channel on freenode I figured out that this limit is arbitrarily. 

That was the reason I opened up the bug report on kernel.org.

https://bugzilla.kernel.org/show_bug.cgi?id=118111

I hope it will be fixed soon.
