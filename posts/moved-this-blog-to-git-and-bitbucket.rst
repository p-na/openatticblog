.. title: Moved this blog to git and BitBucket
.. slug: moved-this-blog-to-git-and-bitbucket
.. date: 2017-02-11 10:09:43 UTC+01:00
.. tags: bitbucket, blog, community, development, git, mercurial
.. category: 
.. link: 
.. description: Explaining the migration of the openATTIC blog from Mercurial to git and moving it to BitBucket
.. type: text
.. author: Lenz Grimmer

This blog is built a static web site, powered and managed by the excellent
`Nikola <https://getnikola.com/>`_ static web site generator.

The motivation for using a static blog was to make writing articles more
straightforward for our developers, without introducing unnecessary media breaks
and barriers. Using this framework, one can contribute content using familiar
tools and workflows.

When we initially created this blog in September 2015, we used `Mercurial
<https://www.mercurial-scm.org/>`_ for managing the source files and assets and
hosted the repository on an internal Mercurial host.

This has now been changed - we moved the source `code repository
<https://bitbucket.org/openattic/openatticblog>`_ to BitBucket, as part of the
`openATTIC umbrella project <https://bitbucket.org/openattic/>`_.

We also converted the repository from Mercurial to `git
<https://www.git-scm.com/>`_ along the way, primarily as an exercise to evaluate
how easy such a conversation can be performed, but also as a first step to give
us a way to practice our git skills and to further open up our development
processes.

The conversion was performed on a local development system using the
`hg-fast-export <https://github.com/frej/fast-export>`_ Python script and was
quite simple, following the steps outlined in the ``README.md`` file::

    $ mkdir openatticblog.git
    $ cd openatticblog.git
    $ git init
    $ ../fast-export/hg-fast-export.py -r ../openatticblog.hg

Afterwards, the git repo could easily be pushed to BitBucket.

BTW, if you take a closer look at this repo, you will notice there's a branch
named ``website``. This is a work in progress project to convert the existing
openattic.org web site from `Typo3 <https://typo3.org/>`_ to Nikola.

While the content is shaping up nicely, the layout and design still needs some
more attention before we will make the switch. If you're familiar with creating
web sites using Nikola and Bootstrap and want to give us a hand, please `let us
know <http://openattic.org/get-involved.html>`_!