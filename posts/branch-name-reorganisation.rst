.. title: Branch Name Reorganisation
.. slug: branch-name-reorganisation
.. date: 2017-04-10 18:32:49 UTC+02:00
.. tags: community, contributing, development, git
.. category: 
.. link: 
.. description: Describing the changes to the git branch names
.. type: text
.. author: Lenz Grimmer

Now that we've :doc:`concluded our migration to git
<openattic-code-repository-migrated-to-git>`, we also made some changes to our
branch naming scheme.

In the Mercurial age, we had two main branches:

* ``development``: the active development branch, used by developers to derive
  their feature branches from and which was used as the target branch for `pull
  requests <https://bitbucket.org/openattic/openattic/pull-requests/>`_.
* ``default``: the branch that nightly release builds and official releases were
  built from. This branch was updated by merging ``development`` into
  ``default`` by Jenkins jobs that performed a wide range of automatic tests
  against the ``development`` branch. Only if these tests had passed, an
  automatic merge to the ``default`` was performed.

During the conversion from Mercurial to git, the ``default`` branch was
automatically renamed to ``master``, which is the canonical name for the "main"
repository branch in git. For most open source projects, the ``master`` branch
resembles the current line of development, similar to how we used the
``development`` branch in Mercurial.

.. TEASER_END

To avoid confusion, we've now adapted the roles of `our git branches
<https://bitbucket.org/openattic/openattic/branches/>`_ in the upstream
repository as follows:

* ``master`` is our latest development version. It's the reference branch for
  developers wanting to work on new features or fixing bugs. Pull requests should
  be submitted against this branch.
* ``stable`` is the branch that is used for nightly builds and official stable
  releases of the latest version (2.0 at the time of writing). New changes only
  get merged from the ``master`` branch if all test pass.

In addition to these two branches, we currently have one additional release
branch in the upstream repository called ``branding-ses4``. This branch contains
the product-specific changes to openATTIC 2.0 as it's used in SUSE Enterprise
Storage Version 4, which relate primarily to UI adaptions (branding and layout
changes). This branch is synced with the ``stable`` branch every time a new
stable release has been cut.