.. title: Meet the openATTIC Team at Chemnitzer Linuxtage 2017
.. slug: meet-the-openattic-team-at-chemnitzer-linuxtage-2017
.. date: 2017-03-10 15:09:30 UTC+01:00
.. tags: community, chemnitz, clt, conference, collaboration, event
.. category:
.. link:
.. description: Announcing our presence at the Chemnitzer Linuxtage
.. type: text
.. author: Lenz Grimmer

.. image:: https://chemnitzer.linux-tage.de/2017/static/img/logo_en.png

This weekend (March 11-12), the `Chemnitzer Linuxtage
<https://chemnitzer.linux-tage.de/2017/en>`_ take place again, and it's our
great pleasure to attend and represent our project there, this time as a proud
member of the growing `openSUSE ecosystem <https://www.opensuse.org/>`_.

We'll be showcasing the latest development version of openATTIC, including the
latest Ceph management features and a preview of the DRBD mirroring
functionality.

In addition to myself, you will be able to meet and talk the following members
of the openATTIC team: Kai Wagner (one of the project founders), Volker Theile
(maintainer of the OpenMediaVault project and also well-known as one of the
early contributors to the FreeNAS project) and Stephan Müller (one of our WebUI
developers).

We look forward to the event and good conversations with the general Linux and
OSS community. If you are attending CLT, please stop by and say hello!