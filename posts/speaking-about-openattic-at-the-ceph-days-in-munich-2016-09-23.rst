.. title: Speaking about openATTIC at the Ceph Days in Munich (2016-09-23)
.. slug: speaking-about-openattic-at-the-ceph-days-in-munich-2016-09-23
.. date: 2016-09-16 10:06:05 UTC+02:00
.. tags: ceph, community, conference, development, openattic, storage
.. category:
.. link:
.. description: Announcing the openATTIC talk at Ceph Days Munich
.. type: text
.. author: Lenz Grimmer

.. image:: http://ceph.com/wp-content/themes/ceph/images/logo_cephdays.png

`Ceph Days <http://ceph.com/cephdays/>`_ are full-day events from and for the
Ceph community which take place around the globe. They usually provide a good
variety of talks, including technical deep-dives, best practices and updates
about recent developments.

The next Ceph Day will `take place in Munich
<http://ceph.com/cephdays/ceph-day-munich/>`_, Germany next week (Friday, 23rd
of September). I'll be there, to give an overview and update about openATTIC,
particularly on the current Ceph management and monitoring feature set as well
as an outlook on ongoing and upcoming developments.

If you're using Ceph and would like to get updates on recent development
"straight from the horse's mouth", next week is your chance! I look forward to
being there.