.. title: Speaking about openATTIC at FOSDEM 2017
.. slug: speaking-about-openattic-at-fosdem-2017
.. date: 2017-01-20 14:37:37 UTC+01:00
.. tags: collaboration, conference, event, fosdem, opensource, community
.. category:
.. link:
.. description: Announcing our FOSDEM 2017 attendence
.. type: text
.. author: Lenz Grimmer

.. image:: https://fosdem.org/2017/assets/style/logo-big-a5243e4d7e00f8bc6816e2b3f3804f505a17ae4832e6e52a24d183617e03a87c.png

On February 4th and 5th, the annual FOSDEM conference will take place in
Brussels, Belgium.

This year, I'll give a talk titled `Ceph and Storage management with openATTIC
<https://fosdem.org/2017/schedule/event/openattic/>`_ in which I'd like to give
an overview and update about our project.

The talk will be a session in the the `Software Defined Storage developer room
<https://fosdem.org/2017/schedule/track/software_defined_storage/>`_ that is
scheduled to take place on Sunday, 5th. My talk takes place at 13:30.

FOSDEM is a very popular and intensive conference - I look forward to attending
it again!